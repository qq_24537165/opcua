package com.rookie.opcua.mapper;

import com.rookie.opcua.entity.Test;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface
TestMapper {
    @Select("SELECT * FROM node_structure")
    public List<Test> Test(String table, String type);


}
