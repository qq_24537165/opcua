package com.rookie.opcua.service;

import com.rookie.opcua.entity.Test;
import com.rookie.opcua.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestService {

    @Autowired
    TestMapper mapper;

    public List<Test> Test(String table, String type) {

        return mapper.Test(table, type);
    }
}
