package com.rookie.opcua.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Test {
    // 增加JsonProperty防止首字母被转为小写

    @JsonProperty("id")
    private Integer id;

}