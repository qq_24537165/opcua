# rookie-opcua
OPC-UA统一架构通信协议与硬件网关plc主机通讯demo,java程序为客户端,使用opc-ua的调试服务器工具模拟进行调试,
订阅方式、轮询方式采集服务端的模拟数据写入redis、mysql数据库，根据自身需要写入opc ua服务器节点数据

### 操作数据库mysql例子
![](./images/4.png)
![](./images/1.png)

### opc ua 官方客户端连接例子
![](./images/2.png)
![](./images/3.png)


### opc-ua 服务器
![](./images/6.png)
![](./images/5.png)
![](./images/9.png)
PS: 服务器端修改值为这样,不然客户端修改不了

### 轮询方式操作数据库mysql例子
![](./images/7.png)

### 订阅模式操作数据库mysql例子
![](./images/8.png)
PS: 当我们使用opc ua官方客户端修改,节点数据,订阅模式会接受触发改变的节点

[模拟器及相关资料某云盘地址](https://pan.baidu.com/s/11JfzuK73fQL48ye1TZDPbA?pwd=vtyf) 提取码: vtyf